import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Principal',
    main: [
      {
        state: 'dashboard',
        short_label: 'Dashboard',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-home'
      },
      // {
      //   state: 'clientes',
      //   short_label: 'Clientes',
      //   name: 'Clientes',
      //   type: 'sub',
      //   icon: 'ti-user',
      //   children: [
      //     {
      //       state: 'clientes-cadastrados',
      //       short_label: 'Clientes Cadastrados',
      //       name: 'Clientes Cadastrados'
      //     },
      //     {
      //       state: 'clientes-adicionar',
      //       short_label: 'Adicionar Cliente',
      //       name: 'Adicionar Cliente'
      //     }
      //   ]
      // },
      {
        state: 'pesquisas',
        short_label: 'Pesquisas',
        name: 'Pesquisas',
        type: 'sub',
        icon: 'ti-tablet',
        children: [
          {
            state: 'pesquisas-em-aberto',
            short_label: 'Pesquisas Em Aberto',
            name: 'Pesquisas Em Aberto'
          },
          {
            state: 'pesquisas-coletadas',
            short_label: 'Pesquisas Coletadas',
            name: 'Pesquisas Coletadas'
          }
        ]
      },
      // {
      //   state: 'localidades',
      //   short_label: 'Localidades',
      //   name: 'Localidades',
      //   type: 'link',
      //   icon: 'ti-world'
      // },
      // {
      //   state: 'tempo-real',
      //   short_label: 'Tempo Real',
      //   name: 'Tempo Real',
      //   type: 'link',
      //   icon: 'ti-map-alt'
      // },
      /*{
        state: 'dispositivos',
        short_label: 'Dispositivos',
        name: 'Dispositivos',
        type: 'sub',
        icon: 'ti-mobile',
        children: [
          {
            state: 'dispositivos-cadastrados',
            short_label: 'Dispositivos Cadastrados',
            name: 'Dispositivos Cadastrados'
          },
          {
            state: 'dispositivos-adicionar',
            short_label: 'Adicionar Dispositivo',
            name: 'Adicionar Dispositivo'
          }
        ]
      },*/
    ],
  },
  // {
  //   label: 'Configurações',
  //   main:[
  //     {
  //       state: 'meu-perfil',
  //       short_label: 'Meu Perfil',
  //       name: 'Meu Perfil',
  //       type: 'link',
  //       icon: 'ti-face-smile'
  //     },
  //     {
  //       state: 'funcionarios',
  //       short_label:'Funcionários',
  //       name: 'Funcionários',
  //       type: 'sub',
  //       icon: 'ti-tag',
  //       children: [
  //         {
  //           state: 'funcionarios-cadastrados',
  //           short_label: 'Funcionários Cadastrados',
  //           name: 'Funcionários Cadastrados'
  //         },
  //         {
  //           state: 'funcionarios-adicionar',
  //           short_label: 'Adicionar Funcionário',
  //           name: 'Adicionar Funcionário'
  //         }
  //       ]
  //     },
  //     {
  //       state: 'grupos',
  //       short_label: 'Grupos',
  //       name: 'Grupos de Permissão',
  //       type: 'sub',
  //       icon: 'ti-ticket',
  //       children: [
  //         {
  //           state: 'grupos-cadastrados',
  //           short_label: 'Grupos Cadastrados',
  //           name: 'Grupos Cadastrados'
  //         },
  //         {
  //           state: 'grupos-adicionar',
  //           short_label: 'Adicionar Grupo',
  //           name: 'Adicionar Grupo'
  //         }
  //       ]
  //     }
  //   ]
  // },
  // {
  //   label: 'Alfa Pesquisas',
  //   main:[
  //     {
  //       state: 'component',
  //       short_label: 'Contato',
  //       name: 'Contato',
  //       type: 'link',
  //       icon: 'ti-support'
  //     }
  //   ]
  // }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
