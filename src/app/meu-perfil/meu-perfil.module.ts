import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeuPerfilRoutingModule } from './meu-perfil-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MeuPerfilComponent } from './meu-perfil.component';

@NgModule({
  imports: [
    CommonModule,
    MeuPerfilRoutingModule,
    SharedModule
  ],
  declarations: [MeuPerfilComponent]
})
export class MeuPerfilModule { }
