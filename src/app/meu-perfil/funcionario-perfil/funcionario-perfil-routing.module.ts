import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FuncionarioPerfilComponent } from './funcionario-perfil.component';

const routes: Routes = [{
  path: '',
  component: FuncionarioPerfilComponent,
  data: {
    breadcrumb: 'Funcionário',
    icon: 'icofont-user bg-c-blue',
    status: false
  }}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FuncionarioPerfilRoutingModule { }
