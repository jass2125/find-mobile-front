import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionarioPerfilComponent } from './funcionario-perfil.component';

describe('FuncionarioPerfilComponent', () => {
  let component: FuncionarioPerfilComponent;
  let fixture: ComponentFixture<FuncionarioPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuncionarioPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionarioPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
