import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuncionarioPerfilRoutingModule } from './funcionario-perfil-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { FuncionarioPerfilComponent } from './funcionario-perfil.component';

@NgModule({
  imports: [
    CommonModule,
    FuncionarioPerfilRoutingModule,
    SharedModule
  ],
  declarations: [FuncionarioPerfilComponent]
})
export class FuncionarioPerfilModule { }
