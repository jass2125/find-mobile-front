import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FisicoComponent } from './fisico.component';

const routes: Routes = [{
  path: '',
  component: FisicoComponent,
  data: {
    breadcrumb: 'Cliente Fisico',
    icon: 'icofont-user bg-c-blue',
    status: false
  }}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FisicoRoutingModule { }
