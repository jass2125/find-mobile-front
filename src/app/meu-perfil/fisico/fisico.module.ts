import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FisicoRoutingModule } from './fisico-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { FisicoComponent } from './fisico.component';

@NgModule({
  imports: [
    CommonModule,
    FisicoRoutingModule,
    SharedModule
  ],
  declarations: [FisicoComponent]
})
export class FisicoModule { }
