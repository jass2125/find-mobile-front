import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import {fadeInOutTranslate} from '../../shared/elements/animation';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-juridico',
  templateUrl: './juridico.component.html',
  styleUrls: ['./juridico.component.scss']
})
export class JuridicoComponent implements OnInit {
  tipu = '';
  switchSenha(tipo){
    //$('.title').slideToggle();
    this.tipu = tipo; 
    console.log(this.tipu)

  }

  lat = 21.1591857;
  lng = 72.7522563;
  latA = 21.7613308;
  lngA = 71.6753074;
  zoom = 8;

  styles: any = [{
    featureType: 'all',
    stylers: [{
      saturation: -80
    }]
  }, {
    featureType: 'road.arterial',
    elementType: 'geometry',
    stylers: [{
      hue: '#00ffee'
    }, {
      saturation: 50
    }]
  }, {
    featureType: 'poi.business',
    elementType: 'labels',
    stylers: [{
      visibility: 'off'
    }]
  }];

  constructor() { }

  ngOnInit() {
  }

  submeter() {
    swal({
      title: 'Alterar cliente?',
      text: 'Os dados serão alterados',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Alterar'
    }).then((result) => {
      if (result.value) {
        swal(
          'Enviado!',
          'Dados alterados com sucesso',
          'success'
        );
      }
    });
  }

  cancelar(){
    swal({
      title: 'Cancelar?',
      text: 'Os dados não serão alterados',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        window.location.href = 'meu-pefil'
      }
    });
  }

}
