import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JuridicoComponent } from './juridico.component';

const routes: Routes = [{
  path: '',
  component: JuridicoComponent,
  data: {
    breadcrumb: 'Cliente Jurídico',
    icon: 'icofont-user bg-c-blue',
    status: false
  }}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JuridicoRoutingModule { }
