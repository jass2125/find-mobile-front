import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JuridicoRoutingModule } from './juridico-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { JuridicoComponent } from './juridico.component';

@NgModule({
  imports: [
    CommonModule,
    JuridicoRoutingModule,
    SharedModule
  ],
  declarations: [JuridicoComponent]
})
export class JuridicoModule { }
