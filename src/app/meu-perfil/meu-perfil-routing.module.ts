import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeuPerfilComponent } from './meu-perfil.component';

const routes: Routes = [
  {
    path: '',
    component: MeuPerfilComponent,
    data: {
      breadcrumb: 'Meu Perfil',
      icon: 'icofont-user bg-c-blue',
      status: false
    },
    children: [
      {
        path: 'fisico',
        loadChildren: './fisico/fisico.module#FisicoModule'
      },
      {
        path: 'juridico',
        loadChildren: './juridico/juridico.module#JuridicoModule'
      },
      {
        path: 'funcionario',
        loadChildren: './funcionario-perfil/funcionario-perfil.module#FuncionarioPerfilModule'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeuPerfilRoutingModule { }

