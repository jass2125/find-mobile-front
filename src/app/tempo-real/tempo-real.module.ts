import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TempoRealRoutingModule } from './tempo-real-routing.module';
import { TempoRealComponent } from './tempo-real.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    TempoRealRoutingModule,
    SharedModule
  ],
  declarations: [TempoRealComponent]
})
export class TempoRealModule { }
