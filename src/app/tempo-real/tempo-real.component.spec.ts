import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempoRealComponent } from './tempo-real.component';

describe('TempoRealComponent', () => {
  let component: TempoRealComponent;
  let fixture: ComponentFixture<TempoRealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempoRealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempoRealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
