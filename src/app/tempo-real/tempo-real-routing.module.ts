import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {TempoRealComponent} from './tempo-real.component';

const routes: Routes = [
  {
    path: '',
    component: TempoRealComponent,
    data: {
      breadcrumb: 'Tempo Real',
      icon: 'icofont-user bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TempoRealRoutingModule { }
