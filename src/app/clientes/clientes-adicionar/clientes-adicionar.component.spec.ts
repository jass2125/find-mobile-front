import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesAdicionarComponent } from './clientes-adicionar.component';

describe('ClientesAdicionarComponent', () => {
  let component: ClientesAdicionarComponent;
  let fixture: ComponentFixture<ClientesAdicionarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesAdicionarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesAdicionarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
