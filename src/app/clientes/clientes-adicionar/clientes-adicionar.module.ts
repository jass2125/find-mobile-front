import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientesAdicionarRoutingModule } from './clientes-adicionar-routing.module';
import { ClientesAdicionarComponent } from './clientes-adicionar.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ClientesAdicionarRoutingModule,
    SharedModule
  ],
  declarations: [ClientesAdicionarComponent]
})
export class ClientesAdicionarModule { }
