import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import swal from 'sweetalert2';
import {fadeInOutTranslate} from '../../shared/elements/animation';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-clientes-adicionar',
  templateUrl: './clientes-adicionar.component.html',
  styleUrls: ['./clientes-adicionar.component.scss']
})
export class ClientesAdicionarComponent implements OnInit {

  tipu = '';
  switchPessoa(tipo){
    //$('.title').slideToggle();
    this.tipu = tipo; 
    console.log(this.tipu)

  }

  lat = 21.1591857;
  lng = 72.7522563;
  latA = 21.7613308;
  lngA = 71.6753074;
  zoom = 8;

  styles: any = [{
    featureType: 'all',
    stylers: [{
      saturation: -80
    }]
  }, {
    featureType: 'road.arterial',
    elementType: 'geometry',
    stylers: [{
      hue: '#00ffee'
    }, {
      saturation: 50
    }]
  }, {
    featureType: 'poi.business',
    elementType: 'labels',
    stylers: [{
      visibility: 'off'
    }]
  }];

  constructor() { }

  ngOnInit() {
  }

  submeter() {
    swal({
      title: 'Cadastrar cliente?',
      text: 'Os dados serão cadastrados',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Cadastrar'
    }).then((result) => {
      if (result.value) {
        swal(
          'Enviado!',
          'Cliente cadastrado com sucesso',
          'success'
        );
      }
    });
  }

  cancelar(){
    swal({
      title: 'Cancelar?',
      text: 'Os dados não serão cadastrados',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        window.location.href = 'clientes/clientes-cadastrados'
      }
    });
  }


}
