import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClientesAdicionarComponent} from './clientes-adicionar.component';

const routes: Routes = [
  {
    path: '',
    component: ClientesAdicionarComponent,
    data: {
      breadcrumb: 'Adicionar Cliente'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientesAdicionarRoutingModule { }
