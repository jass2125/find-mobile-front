import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientesCadastradosRoutingModule } from './clientes-cadastrados-routing.module';
import { ClientesCadastradosComponent } from './clientes-cadastrados.component';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ClientesCadastradosRoutingModule,
    SharedModule,
    HttpModule
  ],
  declarations: [ClientesCadastradosComponent]
})
export class ClientesCadastradosModule { }
