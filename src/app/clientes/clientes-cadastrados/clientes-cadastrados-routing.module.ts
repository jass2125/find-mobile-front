import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClientesCadastradosComponent} from './clientes-cadastrados.component';

const routes: Routes = [
  {
    path: '',
    component: ClientesCadastradosComponent,
    data: {
      breadcrumb: 'Clientes Cadastrados',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientesCadastradosRoutingModule { }
