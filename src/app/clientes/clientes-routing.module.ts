import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClientesComponent} from './clientes.component';

const routes: Routes = [
  {
    path: '',
    component: ClientesComponent,
    data: {
      breadcrumb: 'Clientes',
      icon: 'icofont-user bg-c-blue',
      status: false
    },
    children: [
      {
        path: 'clientes-cadastrados',
        loadChildren: './clientes-cadastrados/clientes-cadastrados.module#ClientesCadastradosModule'
      },
      {
        path: 'clientes-adicionar',
        loadChildren: './clientes-adicionar/clientes-adicionar.module#ClientesAdicionarModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientesRoutingModule { }
