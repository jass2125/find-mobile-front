import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-localidades',
  templateUrl: './localidades.component.html',
  styleUrls: ['./localidades.component.scss']
})
export class LocalidadesComponent implements OnInit {
  @ViewChild('myTable') table: any;

  public rows: any[] = [];
  public expanded: any = {};
  public timeout: any;
  public toggle = 1;

  rowsFilter = [];
  tempFilter = [];

  public rowsOnPage = 10;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';

  constructor() {
    this.fetch((data) => {
      this.rows = data;
    });

    this.fetchFilterData((data) => {
      // cache our list
      this.tempFilter = [...data];

      // push our inital complete list
      this.rowsFilter = data;
    });
  }

  ngOnInit() {}

  fetchFilterData(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/localidades.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.tempFilter.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  onPage(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log('paged!', event);
    }, 100);
  }

  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/localidades.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  toggleExpandRow(row) {
    if(this.toggle = 1){
    this.table.rowDetail.toggleExpandRow(row);
    // console.log('Toggled Expand Row!', row);
    this.toggle = 0;
    }
    else{
      this.toggle = 1;
      this.table.rowDetail.collapseAllRows();
    }
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  adicionar() {
    swal({
      title: 'Adicionar localidade?',
      text: 'Os dados serão cadastrados',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Adicionar'
    }).then((result) => {
      if (result.value) {
        swal(
          'Adicionada!',
          'Localidade adicionada com sucesso',
          'success'
        );
      }
    });
  }

  cancelar(){
    swal({
      title: 'Cancelar?',
      text: 'A localidade não será adicionada',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        window.location.href = 'localidades'
      }
    });
  }

  remover(row,value,rowIndex) {
    swal({
      title: 'Excluir localidade?',
      text: 'Remoção de localidade',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Excluir'
    }).then((result) => {
      if (result.value) {
        //let index = this.data.indexOf(item);
    try {
      //this.data.splice(index, 1);
      console.log('Row: ',row)
      

  } catch (e) {}
      }
    });
}

}
