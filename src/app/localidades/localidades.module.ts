import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocalidadesRoutingModule } from './localidades-routing.module';
import { LocalidadesComponent } from './localidades.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    LocalidadesRoutingModule,
    SharedModule
  ],
  declarations: [LocalidadesComponent]
})
export class LocalidadesModule { }
