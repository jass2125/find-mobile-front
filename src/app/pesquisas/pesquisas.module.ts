import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {SharedModule} from '../shared/shared.module';
import {HttpModule} from '@angular/http';
import { PesquisasRoutingModule } from './pesquisas.routing.module';
import { PesquisasComponent } from './pesquisas.component';
import { PesquisasService } from './pesquisas.service';
import { PesquisasEmAbertoComponent } from './pesquisas-em-aberto/pesquisas-em-aberto.component';
import { PesquisasEmAbertoService } from './pesquisas-em-aberto/pesquisas-em-aberto.service';


@NgModule({
  imports: [
    CommonModule,
    PesquisasRoutingModule,
    SharedModule,
    HttpModule
  ],
  declarations: [PesquisasComponent ],
  providers: [PesquisasService ]
})
export class PesquisasModule { }
