import { TestBed, inject } from '@angular/core/testing';
import { PesquisasColetadasService } from './pesquisas-coletadas.service';

describe('PesquisasColetadasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PesquisasColetadasService]
    });
  });

  it('should be created', inject([PesquisasColetadasService], (service: PesquisasColetadasService) => {
    expect(service).toBeTruthy();
  }));
});
