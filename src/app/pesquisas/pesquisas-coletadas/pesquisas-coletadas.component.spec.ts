import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PesquisasColetadasComponent } from './pesquisas-coletadas.component';

describe('PesquisasColetadasComponent', () => {
  let component: PesquisasColetadasComponent;
  let fixture: ComponentFixture<PesquisasColetadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisasColetadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisasColetadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
