import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PesquisasColetadasComponent } from './pesquisas-coletadas.component';

const routes: Routes = [
  {
    path: '',
    component: PesquisasColetadasComponent,
    data: {
      breadcrumb: 'Pesquisas Coletadas'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PesquisasColetadasRoutingModule { }
