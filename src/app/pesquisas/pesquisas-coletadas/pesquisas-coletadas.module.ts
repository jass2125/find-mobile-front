import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { HttpModule } from '@angular/http';
import { PesquisasColetadasRoutingModule } from './pesquisas-coletadas-routing.module';
import { PesquisasColetadasService } from './pesquisas-coletadas.service';
import { PesquisasColetadasComponent } from './pesquisas-coletadas.component';

@NgModule({
  imports: [
    CommonModule,
    PesquisasColetadasRoutingModule,
    SharedModule,
    HttpModule
  ],
  declarations: [ PesquisasColetadasComponent ],
  providers: [ PesquisasColetadasService ]
})
export class PesquisasColetadasModule { }
