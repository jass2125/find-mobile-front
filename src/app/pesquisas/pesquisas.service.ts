import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams, ResponseContentType, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

declare var saveAs: any;

@Injectable()
export class PesquisasService {

  pesquisasUrl = 'http://localhost:8080/api/pesquisas';

  constructor(private http: Http) { }

  pesquisar(): Promise<any> {
    const params = new URLSearchParams();
    const headers = new Headers();

    return this.http.get(`${this.pesquisasUrl}`,
    { headers, search: params })
    .toPromise()
    .then(response => {
      const responseJson = response.json();

      return responseJson;
    });
  }

  baixarArquivo(pesquisasCodes): Promise<any> {
    const params = new URLSearchParams();

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const options = {responseType: ResponseContentType.ArrayBuffer, headers };

    return this.http.post(`${this.pesquisasUrl}/exportar`, JSON.stringify(pesquisasCodes), options)
    .toPromise()
    .then(response => {
      saveAs(response.blob(), 'pesquisas.pdf');
    });
  }

  excluir(codigo: number): Promise<void> {
    return this.http.delete(`${this.pesquisasUrl}/${codigo}`)
    .toPromise()
    .then(() => null);
  }

  excluirLote(pesquisasCodes): Promise<void> {
    const params = new URLSearchParams();

    //const headers = new Headers();
    //headers.append('Content-Type', 'application/json');

    //const options = { headers, data: JSON.stringify(pesquisasCodes) };

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({
      headers: headers,
      body : JSON.stringify(pesquisasCodes)
    });

    return this.http.delete(`${this.pesquisasUrl}`, options)
    .toPromise()
    .then(() => null);
  }

}
