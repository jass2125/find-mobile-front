import { TestBed, inject } from '@angular/core/testing';
import { PesquisasEmAbertoService } from './pesquisas-em-aberto.service';

describe('PesquisasEmAbertoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PesquisasEmAbertoService]
    });
  });

  it('should be created', inject([PesquisasEmAbertoService], (service: PesquisasEmAbertoService) => {
    expect(service).toBeTruthy();
  }));
});
