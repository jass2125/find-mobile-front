import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PesquisasEmAbertoComponent } from './pesquisas-em-aberto.component';

const routes: Routes = [
  {
    path: '',
    component: PesquisasEmAbertoComponent,
    data: {
      breadcrumb: 'Pesquisas Em Aberto'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PesquisasEmAbertoRoutingModule { }
