import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { HttpModule } from '@angular/http';
import { PesquisasEmAbertoComponent } from './pesquisas-em-aberto.component';
import { PesquisasEmAbertoService } from './pesquisas-em-aberto.service';
import { PesquisasEmAbertoRoutingModule } from './pesquisas-em-aberto-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PesquisasEmAbertoRoutingModule,
    SharedModule,
    HttpModule
  ],
  declarations: [ PesquisasEmAbertoComponent ],
  providers: [ PesquisasEmAbertoService ]
})
export class PesquisasEmAbertoModule { }
