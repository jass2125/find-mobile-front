import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PesquisasEmAbertoComponent } from './pesquisas-em-aberto.component';

describe('PesquisasEmAbertoComponent', () => {
  let component: PesquisasEmAbertoComponent;
  let fixture: ComponentFixture<PesquisasEmAbertoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisasEmAbertoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisasEmAbertoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
