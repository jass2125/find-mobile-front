import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import { PesquisasEmAbertoService } from './pesquisas-em-aberto.service';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-pesquisas-em-aberto',
  templateUrl: './pesquisas-em-aberto.component.html',
  styleUrls: ['./pesquisas-em-aberto.component.scss']
})
export class PesquisasEmAbertoComponent implements OnInit {

  public data: any[];
  public rowsOnPage = 10;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';

  rowsBasic = [];
  fullScreenRow = [];
  loadingIndicator = true;
  reorderable = true;

  columns = [
    { prop: 'name' },
    { name: 'Gender' },
    { name: 'Company' }
  ];

  rows = [];
  selected = [];

  timeout: any;

  rowsFilter = [];
  tempFilter = [];

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private operadoresService: PesquisasEmAbertoService) {
  }

  ngOnInit() {
    this.operadoresService.pesquisar()
      .then((data) => {
        console.log(data);

        // push our inital complete list
        this.data = data;
        this.rowsFilter = data;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.data.filter(function(d) {
      return d.nome.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  onActivate(event) {}

  add() {
    this.selected.push(this.rows[1], this.rows[3]);
  }

  update() {
    this.selected = [this.rows[1], this.rows[3]];
  }

  remove() {
    this.selected = [];
  }

  remover(row, value, rowIndex) {
    swal({
      title: 'Excluir operador?',
      text: 'Remoção de operador',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Excluir'
    }).then((result) => {
      if (result.value) {
        this.operadoresService.excluir(row.id).then(() => {
          this.data = this.data.filter((el) => el.id !== row.id);
          this.rowsFilter = this.rowsFilter.filter((el) => el.id !== row.id);
          swal('Excluir operador', 'Operador removido com sucesso', 'success');
        }).catch((error) => {
          console.log(error);
          swal({
            title: 'Excluir operador',
            text: 'Falha ao remover operador',
            type: 'warning',
            showCancelButton: true,
            showConfirmButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Ok'
          });
        }).then((res) => {
          this.remove();
        });
        console.log('Row: ', row);
        this.selected = [];
      }
    });
}

  remove_selected() {
    if (this.selected.length === 0) {
      swal({
        title: 'Excluir operadores',
        text: 'Selecione os operadores que deseja excluir',
        type: 'warning',
        showCancelButton: true,
        showConfirmButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Ok'
      });
    } else {
    swal({
      title: 'Excluir operadores?',
      text: 'Remoção de operadores selecionados',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Excluir'
    }).then((result) => {
      if (result.value) {
        // let index = this.data.indexOf(item);
    try {
      // this.data.splice(index, 1);
      // console.log('Elementos: ', this.selected);
      const request = {
        operadores:  this.selected.map(function(elem) {
          return {id: elem.id};
        })
      };
      const ids = this.selected.map(function(elem) {
        return {id: elem.id};
      });

      this.operadoresService.excluirLote(request).then(() => {
        this.data = this.data.filter((el) => ids.indexOf(el.id) < 0);
        this.rowsFilter = this.rowsFilter.filter((el) => ids.indexOf(el.id) < 0);
        swal('Excluir operadores/', 'Operadores removidos com sucesso', 'success');
      }).catch((error) => {
        console.log(error);
        swal({
          title: 'Excluir operadores',
          text: 'Falha ao remover operadores',
          type: 'warning',
          showCancelButton: true,
          showConfirmButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Ok'
        });
      }).then((res) => {
        this.remove();
      });
      // console.log('Row: ', row);
      // this.selected = [];

      // this.remove();
  } catch (e) {}
      }
    });
  }
}

download_selected() {
    if (this.selected.length === 0) {
      swal({
        title: 'Download de operadores',
        text: 'Selecione os operadores que realizar o download',
        type: 'warning',
        showCancelButton: true,
        showConfirmButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Ok'
      });
    } else {
      const request = {
        operadores:  this.selected.map(function(elem) {
          return {id: elem.id};
        })
      };

      console.log(JSON.stringify(request));
      this.operadoresService.baixarArquivo(request).catch((error) => {
        console.log(error);
        swal({
          title: 'Download de operadores',
          text: 'Falha fazer o download dos operadores',
          type: 'warning',
          showCancelButton: true,
          showConfirmButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Ok'
        });
      });
  }
}

details(item) {
  const index = this.data.indexOf(item);
  window.location.href = 'funcionarios/funcionarios-cadastrados/detalhes';
}

}
