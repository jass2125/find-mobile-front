import { TestBed, inject } from '@angular/core/testing';
import { PesquisasService } from './pesquisas.service';

describe('PesquisasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PesquisasService]
    });
  });

  it('should be created', inject([PesquisasService], (service: PesquisasService) => {
    expect(service).toBeTruthy();
  }));
});
