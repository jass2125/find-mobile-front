import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PesquisasComponent } from './pesquisas.component';
import { PesquisasEmAbertoComponent } from './pesquisas-em-aberto/pesquisas-em-aberto.component';

const routes: Routes = [
  {
    path: '',
    component: PesquisasComponent,
    data: {
      breadcrumb: 'Pesquisas',
      icon: 'icofont-user bg-c-blue',
      status: false
    }, 
    children : [
      {
        path: 'pesquisas-em-aberto', 
        loadChildren: './pesquisas-em-aberto/pesquisas-em-aberto.module#PesquisasEmAbertoModule',
      },
      {
        path: 'pesquisas-coletadas', 
        loadChildren: './pesquisas-coletadas/pesquisas-coletadas.module#PesquisasColetadasModule',
      }
    ]
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PesquisasRoutingModule { }
