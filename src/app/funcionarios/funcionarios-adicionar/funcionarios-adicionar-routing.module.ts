import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FuncionariosAdicionarComponent} from './funcionarios-adicionar.component';

const routes: Routes = [
  {
    path: '',
    component: FuncionariosAdicionarComponent,
    data: {
      breadcrumb: 'Adicionar Funcionario',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FuncionariosAdicionarRoutingModule { }
