import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionariosAdicionarComponent } from './funcionarios-adicionar.component';

describe('FuncionariosAdicionarComponent', () => {
  let component: FuncionariosAdicionarComponent;
  let fixture: ComponentFixture<FuncionariosAdicionarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuncionariosAdicionarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionariosAdicionarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
