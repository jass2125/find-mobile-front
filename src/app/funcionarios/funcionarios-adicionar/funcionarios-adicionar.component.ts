import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-funcionarios-adicionar',
  templateUrl: './funcionarios-adicionar.component.html',
  styleUrls: ['./funcionarios-adicionar.component.scss']
})

export class FuncionariosAdicionarComponent implements OnInit {
  simpleOption: any;
  selectedOption = '3';
  isDisabled = true;
  selectedCharacter = '3';
  timeLeft = 5;

  lat = 21.1591857;
  lng = 72.7522563;
  latA = 21.7613308;
  lngA = 71.6753074;
  zoom = 8;

  styles: any = [{
    featureType: 'all',
    stylers: [{
      saturation: -80
    }]
  }, {
    featureType: 'road.arterial',
    elementType: 'geometry',
    stylers: [{
      hue: '#00ffee'
    }, {
      saturation: 50
    }]
  }, {
    featureType: 'poi.business',
    elementType: 'labels',
    stylers: [{
      visibility: 'off'
    }]
  }];

  constructor() { }

  ngOnInit() {
    this.simpleOption = [{
      "id": 106,
      "group": "Group 1",
      "label": "Item 1"
  },
  {
      "id": 107,
      "group": "Group 1",
      "label": "Item 2"
  }
]

  }
  
  submeter() {
    swal({
      title: 'Cadastrar funcionário?',
      text: 'Os dados serão cadastrados',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Cadastrar'
    }).then((result) => {
      if (result.value) {
        swal(
          'Enviado!',
          'Funcionário cadastrado com sucesso',
          'success'
        );
      }
    });
  }

  cancelar(){
    swal({
      title: 'Cancelar?',
      text: 'Os dados não serão cadastrados',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        window.location.href = 'funcionarios/funcionarios-cadastrados'
      }
    });
  }

}
