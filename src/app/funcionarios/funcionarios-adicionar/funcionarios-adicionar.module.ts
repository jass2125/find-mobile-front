import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuncionariosAdicionarRoutingModule } from './funcionarios-adicionar-routing.module';
import { FuncionariosAdicionarComponent } from './funcionarios-adicionar.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FuncionariosAdicionarRoutingModule,
    SharedModule
  ],
  declarations: [FuncionariosAdicionarComponent]
})
export class FuncionariosAdicionarModule { }
