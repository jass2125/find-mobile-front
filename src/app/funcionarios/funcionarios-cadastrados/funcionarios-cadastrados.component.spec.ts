import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionariosCadastradosComponent } from './funcionarios-cadastrados.component';

describe('FuncionariosCadastradosComponent', () => {
  let component: FuncionariosCadastradosComponent;
  let fixture: ComponentFixture<FuncionariosCadastradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuncionariosCadastradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionariosCadastradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
