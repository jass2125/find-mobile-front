import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuncionariosCadastradosRoutingModule } from './funcionarios-cadastrados-routing.module';
import { FuncionariosCadastradosComponent } from './funcionarios-cadastrados.component';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FuncionariosCadastradosRoutingModule,
    SharedModule,
    HttpModule
  ],
  declarations: [FuncionariosCadastradosComponent]
})
export class FuncionariosCadastradosModule { }
