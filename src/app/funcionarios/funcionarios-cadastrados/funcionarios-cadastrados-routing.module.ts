import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FuncionariosCadastradosComponent} from './funcionarios-cadastrados.component';

const routes: Routes = [
  {
    path: '',
    component: FuncionariosCadastradosComponent,
    data: {
      breadcrumb: 'Funcionarios Cadastrados',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FuncionariosCadastradosRoutingModule { }
