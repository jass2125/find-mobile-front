import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import swal from 'sweetalert2';
import {fadeInOutTranslate} from '../../shared/elements/animation';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styleUrls: ['./detalhes.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [fadeInOutTranslate]
})
export class DetalhesComponent implements OnInit {

  simpleOption: any;
  selectedOption = '3';
  isDisabled = true;
  selectedCharacter = '3';
  timeLeft = 5;

  lat = 21.1591857;
  lng = 72.7522563;
  latA = 21.7613308;
  lngA = 71.6753074;
  zoom = 8;

  styles: any = [{
    featureType: 'all',
    stylers: [{
      saturation: -80
    }]
  }, {
    featureType: 'road.arterial',
    elementType: 'geometry',
    stylers: [{
      hue: '#00ffee'
    }, {
      saturation: 50
    }]
  }, {
    featureType: 'poi.business',
    elementType: 'labels',
    stylers: [{
      visibility: 'off'
    }]
  }];

  constructor() { }

  ngOnInit() {
    this.simpleOption = [{
      "id": 106,
      "group": "Group 1",
      "label": "Item 1"
  },
  {
      "id": 107,
      "group": "Group 1",
      "label": "Item 2"
  }
]

  }

  submeter() {
    swal({
      title: 'Salvar alterações?',
      text: 'Os dados anteriores serão substituídos',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Salvar'
    }).then((result) => {
      if (result.value) {
        swal(
          'Enviado!',
          'Alterações salvas com sucesso',
          'success'
        );
      }
    });
  }

  cancelar(){
    swal({
      title: 'Cancelar?',
      text: 'As alterações não serão salvas',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        window.location.href = 'funcionarios/funcionarios-cadastrados'
      }
    });
  }

}
