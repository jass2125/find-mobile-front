import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DetalhesComponent} from './detalhes.component';

const routes: Routes = [
  {
    path: '',
    component: DetalhesComponent,
    data: {
      breadcrumb: 'Funcionario - Fernando',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetalhesRoutingModule { }
