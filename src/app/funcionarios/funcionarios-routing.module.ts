import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FuncionariosComponent} from './funcionarios.component';

const routes: Routes = [
  {
    path: '',
    component: FuncionariosComponent,
    data: {
      breadcrumb: 'Funcionarios',
      icon: 'icofont-user bg-c-blue',
      status: false
    },
    children: [
      {
        path: 'funcionarios-cadastrados',
        loadChildren: './funcionarios-cadastrados/funcionarios-cadastrados.module#FuncionariosCadastradosModule'
      },
      {
        path: 'funcionarios-adicionar',
        loadChildren: './funcionarios-adicionar/funcionarios-adicionar.module#FuncionariosAdicionarModule'
      },
      {
        path: 'funcionarios-cadastrados/detalhes',
        loadChildren: './detalhes/detalhes.module#DetalhesModule'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FuncionariosRoutingModule { }
