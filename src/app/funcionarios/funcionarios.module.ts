import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuncionariosRoutingModule } from './funcionarios-routing.module';
import { FuncionariosComponent } from './funcionarios.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FuncionariosRoutingModule,
    SharedModule
  ],
  declarations: [FuncionariosComponent]
})
export class FuncionariosModule { }
