import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GruposComponent } from './grupos.component';

const routes: Routes = [{
  path: '',
  component: GruposComponent,
  data: {
    breadcrumb: 'Grupos de Permissões',
    icon: 'icofont-user bg-c-blue',
    status: false
  },
  children: [
    {
      path: 'grupos-cadastrados',
      loadChildren: './grupos-cadastrados/grupos-cadastrados.module#GruposCadastradosModule'
    }/*,
    {
      path: 'grupos-adicionar',
      loadChildren: './grupos-adicionar/grupos-adicionar.module#GruposAdicionarModule'
    }*/
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GruposRoutingModule { }
