import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GruposRoutingModule } from './grupos-routing.module';
import { SharedModule } from '../shared/shared.module';
import { GruposComponent } from './grupos.component';

@NgModule({
  imports: [
    CommonModule,
    GruposRoutingModule,
    SharedModule
  ],
  declarations: [GruposComponent]
})
export class GruposModule { }
