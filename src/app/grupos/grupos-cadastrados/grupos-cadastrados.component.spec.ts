import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GruposCadastradosComponent } from './grupos-cadastrados.component';

describe('GruposCadastradosComponent', () => {
  let component: GruposCadastradosComponent;
  let fixture: ComponentFixture<GruposCadastradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GruposCadastradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GruposCadastradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
