import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GruposCadastradosRoutingModule } from './grupos-cadastrados-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { GruposCadastradosComponent } from './grupos-cadastrados.component';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    GruposCadastradosRoutingModule,
    SharedModule,
    HttpModule
  ],
  declarations: [GruposCadastradosComponent]
})
export class GruposCadastradosModule { }
