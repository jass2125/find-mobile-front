import { Component, OnInit, ViewChild } from '@angular/core';
import {Http} from '@angular/http';
import swal from 'sweetalert2';
import {DatatableComponent} from '@swimlane/ngx-datatable';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-grupos-cadastrados',
  templateUrl: './grupos-cadastrados.component.html',
  styleUrls: ['./grupos-cadastrados.component.scss']
})
export class GruposCadastradosComponent implements OnInit {

  public data: any[];
  public rowsOnPage = 10;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';

  rowsBasic = [];
  fullScreenRow = [];
  loadingIndicator = true;
  reorderable = true;

  columns = [
    { prop: 'name' },
    { name: 'Gender' },
    { name: 'Company' }
  ];

  rows = [];
  selected = [];

  timeout: any;

  rowsFilter = [];
  tempFilter = [];

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private http: Http) { 

    this.fetchFilterData((data) => {
      // cache our list
      this.tempFilter = [...data];

      // push our inital complete list
      this.rowsFilter = data;
    });
  }

  ngOnInit() {
    this.http.get(`assets/data/grupos.json`)
      .subscribe((data) => {
        this.data = data.json();
      });
  }

  fetchFilterData(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/grupos.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.tempFilter.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  onActivate(event) {}

  add() {
    this.selected.push(this.rows[1], this.rows[3]);
  }

  update() {
    this.selected = [this.rows[1], this.rows[3]];
  }

  remove() {
    this.selected = [];
  }

  
  remover(row,value,rowIndex) {
    swal({
      title: 'Excluir grupo?',
      text: 'Remoção de grupo',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não',
      confirmButtonText: 'Excluir'
    }).then((result) => {
      if (result.value) {
        //let index = this.data.indexOf(item);
    try {
      //this.data.splice(index, 1);
      console.log('Row: ',row)
      

      this.remove();
  } catch (e) {}
      }
    });
}

details(item){
  let index = this.data.indexOf(item);
  window.location.href = 'funcionarios/funcionarios-cadastrados/detalhes'
}




}
