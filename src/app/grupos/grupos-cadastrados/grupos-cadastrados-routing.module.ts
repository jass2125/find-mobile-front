import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GruposCadastradosComponent } from './grupos-cadastrados.component';

const routes: Routes = [
  {
    path: '',
    component: GruposCadastradosComponent,
    data: {
      breadcrumb: 'Grupos Cadastrados',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GruposCadastradosRoutingModule { }
