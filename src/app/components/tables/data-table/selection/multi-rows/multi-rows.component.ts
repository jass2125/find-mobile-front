import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multi-rows',
  templateUrl: './multi-rows.component.html',
  styleUrls: [
    './multi-rows.component.scss'
  ]
})
export class MultiRowsComponent implements OnInit {
  rows = [];
  selected = [];

  columns: any[] = [
    { prop: 'name'} ,
    { name: 'id' },
    { name: 'email' },
    {name: 'document'},
    {name: 'status'},
    {name: 'detalhes'},
    {name: 'visualizar'},
    {name: 'deletar'}
  ];

  constructor() {
    this.fetch((data) => {
      this.rows = data;
    });
  }

  ngOnInit() {
  }

  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/clientes.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  onActivate(event) {}

}
